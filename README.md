# fr-conf-test-sec-cloud
FR - Supports de présentation en français pour le test de sécurité nuagique

## à propos de cette présentation
En 2019, la complexité des logiciels s’accélère. Découplage, polyglottisme, architecture par événements, infrastructure hybride, équipes devops - toute la chaîne de production a profondément changé.

Le test logiciel a amorcé sa mutation dès l’adoption des approches agiles, avec TU, TDD, BDD. Leur automatisation progresse. Pourtant, même morcelé, le logiciel continue à se complexifier. Les itérations raccourcissent, les besoins business s’accélèrent.

La cyber-sécurité passe souvent au second plan, remise à plus tard. Elle sera prise en compte a posteriori, dans une architecture déjà en place. Ni testée ni automatisée, elle continue à être dé-priorisée, au désespoir des RSSI. Jusqu’au jour où survient l’imprévu.

Nous verrons dans cette présentation comment intégrer au plus tôt les tests de sécurité, et aborderons quelques outils.

## support de présentation

Document unique de diaporama en PDF : [Tests de sécurité nuagique.pdf](./Tests%20de%20sécurité%20nuagique.pdf)

## sources et documentation

Les études et documents cités ou ayant servi à ce travail sont disponibles en PDF sur [presentations/sources](https://drive.google.com/open?id=1P9nut3-c6ZVQW_wc_Klq0owCwjO4A8zG)


## liens complémentaires

* [Annonce de la conférence du mardi 5 février 2019](
https://www.linkedin.com/feed/update/urn:li:activity:6496771711776755713)
